﻿using Empresa_Api.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Empresa_Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EnterprisesController : ControllerBase
    {
        private readonly EnterpriseContext _context;

        public EnterprisesController(EnterpriseContext context)
        {
            _context = context;
        }

        [HttpGet("enterprises/{enterprise_types}&{name}")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<Enterprise>>> GetEnterprises(int? enterprise_types, string name)
        {
            var enterprises = await _context.Enterprise.ToListAsync();

            if (enterprise_types != null)
            {
                enterprises = enterprises.Where(x => x.EnterpriseType.Id == enterprise_types).ToList();
                if (name != null)
                    enterprises = enterprises.Where(x => x.Name == name).ToList();
            }
            else if (name != null)
                enterprises = enterprises.Where(x => x.Name == name).ToList();

            return Ok(enterprises);
        }

        [HttpGet("enterprises/details")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<string>>> GetEnterprisesDetails()
        {
            var enterpriseList = await _context.Enterprise.ToListAsync();
            var enterpriseDetailsList = enterpriseList.Select(x => x.Description).ToList();

            return Ok(enterpriseDetailsList);
        }

        [HttpGet("enterprises/{id}")]
        [Authorize]
        public async Task<ActionResult<Enterprise>> GetEnterprise(Guid id)
        {
            var enterprise = await _context.Enterprise.FindAsync(id);

            if (enterprise == null)
            {
                return NotFound();
            }

            return Ok(enterprise);
        }

        [HttpGet("enterprises/{id}/details")]
        [Authorize]
        public async Task<ActionResult<string>> GetEnterpriseDetails(Guid id)
        {
            var enterprise = await _context.Enterprise.FindAsync(id);

            if (enterprise == null)
            {
                return NotFound();
            }

            return Ok(enterprise.Description);
        }
    }
}