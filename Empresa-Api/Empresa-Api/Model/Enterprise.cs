﻿using Microsoft.EntityFrameworkCore;
using System;

namespace Empresa_Api.Model
{
    public class EnterpriseContext : DbContext
    {
        public DbSet<Enterprise> Enterprise { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite("Data Source=EmpresaApi.db");
    }

    public class Enterprise
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public Enterprise_Type EnterpriseType { get; set; }
    }

    public class Enterprise_Type
    {
        public int Id { get; set; }
        public string EnterpriseTypeName { get; set; }
    }
}